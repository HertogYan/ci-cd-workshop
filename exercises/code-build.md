# Building and package validation

'Building' code is mostly converting source code, in a programming language like Java or JavaScript, into a distributable binary artifact. 
This artifact can then uploaded to an application/web server, wrapped into a Docker image and ran, installed to a virtual machine or otherwise deployed. 

Commonly, when 'building' your code into an artifact, there's open source libraries or packages to be included. 
These are pre-built miniature 'programmes' if you will, performing certain common functions so a developer doesn't have to build this from scratch.

These open source packages can get outdated rather quickly, and vulnerabilities can be found in older / unupdated versions of these. 
Therefore, we'll also add a step to our pipeline that checks for any vulnerabilities in the open source packages we use. 

## Goals

Have our pipeline create a distributable binary from our source code that can be deployed. 
At the same time, have it validate that we do not use any outdated open source (third party) packages.

## Approach

Because this is a NodeJS application there is no need for actual compilation (as compared to Java --> .jar/.war). 
To make it distributable / deployable, we'll settle for creating a .zip file.

For auditing open source packages, we're using the **npm audit** command. 
Read more about that [here](https://docs.npmjs.com/getting-started/running-a-security-audit) if you're interested.

## Exercise

First we create a zip that can distributed through an artifact store and can be used 
with a deployment.

### Step 1: The build stage

Create a build stage & build_zip job. This build stage will be the second step in the continuous delivery pipeline. Add the following parts to [.gitlab-ci.yml](../.gitlab-ci.yml) and move to step 2.

```yaml
stages:
# Disable
#  - sample
  - qa
  - build

# The build job
build_zip:
  stage: build
  before_script: []
  image: brandography/alpine-zip
  script:
    - zip lambda src/index.js
  artifacts:
    paths:
    - lambda.zip
    expire_in: 1 week
```

The stage **build** is the new logical divider of steps, or phase, within the CD pipeline. The job **build_zip** is 
actual command that starts the building (bundling) of the application. 


### Step 2: Validate the packages

Next, we're adding the open source security validation to our pipeline. This can be done with **npm audit**; 
this will validate whether the dependencies (and thus our application) are safe for use.

Add the following to the [.gitlab-ci.yml](../.gitlab-ci.yml). We're executing a second job within the new `build` stage.

```yaml
security_audit:
  stage: build
  before_script: []
  script:
  - npm audit
```

### Step 3: Watch your changes live

Similar to last time(s), enter a relevant commit message and commit your changes. This will automatically trigger another pipeline to run!
Watch it go from the pipeline overview, and perhaps dive into some of the separate jobs to view their logs and see exactly what they do. 

------

**Successful?** 
Congrats! Time to move on to exercise 5: [Functional Testing](../exercises/functional-testing.md). Arguably one of the
most important steps! 