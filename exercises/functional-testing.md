# Functional testing

Functional testing is validating if the software is doing what it's supposed to do from a functional perspective.
There's a lot of different possible levels to testing, see below for more info.   

## Goals

![Automated test pyramid](images/automated-testing-pyramid.png)

The test-pyramid is a great basis/strategy for testing. On the lowest section of this pyramid, unit tests are defined:
Tests that do not require a running application as they're testing directly against our source code. These
tests should be very fast and should cover about 90% of the application as they're 'cheap' to write and easy to run, usually
testing single class methods and the like.  

The middle layer can consist of different types of tests, like integration tests that either test a combined flow of several methods/classes,
and/or the edge of your application together with external integrations (hence the name). The top part of the pyramid is 
E2E testing; these are usually the most expensive / hard to write and run. More on that in later exercises!

In high-performing DevOps teams, manual testing is only used to answer the following:

- Does it look right (UI)
- Does it *feel* right (UX)
- Focus on change (do our new automated tests fit?)

These things are hard to understand for a computer. Anything else should immediately be automated and executed on every commit.

## Approach

For unit testing and integration testing there are different approaches. In this exercise, unit testing is done with the 
[Jest framework](https://jestjs.io/), while integration testing is done using a Docker Container* that has a AWS lambda 
architecture.

## Exercise

Our task now is to create a test stage with two functional testing jobs. The first job is the unit test job that is run 
with jest. The second job is AWS docker container that simulates a AWS lambda integration.

### Step 1: Unit testing

Create a `test` stage with a `unit_test` job in the continuous delivery pipeline. Add them to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) like so:

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - deploy

# The unit test job
unit_test:
  stage: test
  script:
  - npm test
```

The stage **test** is an new logical divider of steps within the continuous delivery pipeline. The job **unittest** is 
actual command that starts the unit testing of the application. 

### Step 2: Integration testing

Create a integration test job in the continuous delivery pipeline. Add the following part to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
# The integration job
integration_test:
  stage: test
  before_script: []
  image: docker:dind
  services:
  - name: docker:dind
    alias: docker
  script:
  - "docker run --rm -v \"$PWD/src\":/var/task lambci/lambda:nodejs8.10 index.handler '{\"side1\" : 1, \"side2\" : 2, \"side3\" : 4}'"

```
A new job integration_test is added to the test stage, congrats!

-------

### Step 3: Verify results & what's next
Once again check out your pipeline overview for what it's now doing! If everything went well, move on to our next phase:
[exercise 6: Mock Deploy](../exercises/mock-deploy.md)


*Want to know more about Docker? Let us know during the workshop! 