# (Mock) deploy

Deployment is delivering our binary artifacts (package/application) and our infrastructure in a desired end-state to an environment.
Essentially, we're 'installing' our newly changed software to a server / environment where it can be used. 

## Goals

Ensure our application is installed to a particular environment. In most companies, these environments can be a variation on the following (DTAP/OTAP principle):
* Development
* Test
* Acceptance / QA (or Pre-Production)
* Production 

Sadly, in this setup an actual deployment is not yet an option; We will be doing a mock (fake) deployment for now just
so we get a sense for when this could happen in a regular pipeline.  

## Approach

Create a mock deploy stage & job in our pipeline prior to functional testing stages.

## Exercise

We're adding another stage & job to our `.gitlab-ci.yml` file. 

### Step 1: The mock deployment

Create a mock_deploy job and deploy and add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - deploy


# The mock deployment job
mock_deploy:
  stage: deploy
  script:
  - echo "This is a mock deploy"
```

The stage **deploy** is an new logical divider of steps within the continuous delivery pipeline. The job **mock_deploy** is 
actual command that starts the (fake) deployment of the application. 

### Step 2: Verify results and move on
Like before, check out what your pipeline is now doing, before moving on to [exercise 7: Smoke Testing](../exercises/smoke-test.md).
