# Performance testing

Performance testing is validating how the application is performing. E.g. how fast does it respond on average,
and would our customers be OK with that performance? Has our change introduced a significant performance change,
for better or worse? 

## Goals

Performance testing has four purposes:

- Find a breaking point of an application
- How does the application behave under stress
- What kind of sizing does the application need (CPU, I/O etc.)
- How does the application perform under stress and normal load

## Approach

Performance testing this application will make use of a framework called [artillery](https://artillery.io/). The 
definitions of our performance tests can be found in directory [performance](../performance). This definition makes a 
REST (API) call on the endpoint and captures such data as response time and more.

Within the [package.json](../package.json) file we have a performance test section defined. This will call the artillery 
framework when we tell `npm` to `run performance`. 
 
## Exercise

We're going to setup a performance test job that tells us more about the performance of our application! 

### Step 1: Performance test

Create a performance job and a performance stage in our pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - deploy
  - smoke
  - uat
  - performance

# The performance test job
performance:
  stage: performance
  script:
  - npm run performance
```

The stage **performance** is a new logical divider of steps within the continuous delivery pipeline. The job **performance** is 
actual command that starts the performance test.

### Step 2: Verify results

Once you've added these, commit your change with a relevant message and go to the pipeline overview to see your work!

### What's next?
With this step working, you've reached the end of this workshop! 

What did you think? Any things you'd like to see added, 
or things you'd like to research within your own software processes? Let us know! 

In this workshop, you've worked on automating a lot of processes that would otherwise have to be done manually. This is extremely cost-heavy, 
