# User acceptance testing

User acceptance testing (aka end-to-end testing) is validating our software from a user perspective. 
E.g. does our application now do what the customer wanted? Are we meeting the requirements for new functionality,
and did we do so without breaking any previous functionality (aka create a 'regression')?

This is mostly done manually by an actual user / business representative, but with good collaboration & communication
with the business reps in your process this could be fully replaced by automated testing (for the most part).

## Goals

We will validate the application from the user perspective. We're essentially testing this
by using the application as it is to be used by actual users, with some tweaks here and there.

## Approach

The user acceptance tests (end-to-end/e2e) are done with the JavaScript implementation of the 
[cucumber framework](https://github.com/cucumber/cucumber-js). This framework allows for tests to be written in human 
AND machine readable **given-when-then** statements. This allows us to write test cases that business reps can also
understand, and 'underwater' implement test cases with actual coding by devs/testers. 

The directory [features](../features) contains the user acceptance tests for this application. Check the 
[triangle-calculation.feature](../features/triangle-calculation.feature) file for our business-readable definitions, 
and the [step_definitions](../features/step_definitions) folder for the actual JavaScript running these tests. 

See how this could benefit both business reps ('users') and dev teams? 

## Exercise

In this exercise we're creating a user-acceptance test stage with a end-to-end test job, executing our `cucumber` tests. 
In [package.json](../package.json) there's already an **e2e** step defined that calls the cucumber tests with `npm run e2e`. 
These tests will validate the deployed endpoint.

### Step 1: End-to-end testing

Create an e2e job and a uat stage in the continuous delivery pipeline. Add it to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) like so:

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - deploy
  - smoke
  - uat

# The user acceptance end-to-end test job
e2e:
  stage: uat
  script:
  - npm run e2e
```

The stage **uat** is now our new logical divider of steps within the continuous delivery pipeline. The job **e2e** is 
actual command that starts the end-to-end testing of the application. 

### Step 2: Verify your results

### Step 3: Bonus question & next up
There are some pitfalls to e2e testing. Keeping the testing pyramid from [exercise 5](../exercises/functional-testing.md)
in mind, can you name a few? How could you account for those? 

Congrats, you've completed your user-acceptance testing! 
Move on to the last exercise: [Performance Testing!](../exercises/performance-test.md)
Almost there! 



